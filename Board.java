public class Board {

	private int rows=3, columns = 3;
	private char[][] grid= new char[rows][columns];
	private int x, y, winCounter = 0;
	private Display display = new Display();
	private Position position = new Position();

	protected void main_method() {
		fillBoard();
		display.printBoard(grid);
		do{
			putInBoard();
		}while( win(grid)!= true && checkDraw(winCounter)!= true );
		display.newGameMessage();
		if(display.playNewGame()){
			winCounter = 0;
			main_method();
		}
	}

	public void fillBoard(){
		for(int i=0; i<rows;i++){
			for(int j=0; j<columns; j++){
				grid[i][j]= ' ';
			}
		}
    }

	public void putInBoard(){
		do{
			display.message();
			display.get_input(grid);
			position.findPosition(display.getX(), display.getY());
		}while(check_x(display.getX()) != true || check_y(display.getY())!=true ||
		checkIfValidMove(grid)!=true );

		if(display.getCounter() % 2 ==0){
		grid[position.getX()][position.getY()] = 'X';
		}else{grid[position.getX()][position.getY()] = 'O';}

		display.printBoard(grid);
		winCounter++;
	}

	public boolean check_x(char x){
		if(x== 'a' || x== 'b' || x== 'c'  || x== 'A' || x== 'B' || x== 'C'){
		return true;
		}else{
			display.outOfRangeChar(grid);
			return false;
		}
	}

	public boolean check_y(char y){
		if(y=='1' || y=='2' || y=='3'){
		return true;
		}else{
			display.outOfRangeInt(grid);
			return false;
		}
	}

	public boolean win(char[][] grid){

		if(grid[0][0]==grid[0][1] && grid[0][1]==grid[0][2] && grid[0][0] != ' '||
	       grid[1][0]==grid[1][1] && grid[1][1]==grid[1][2] && grid[1][0] != ' '||
	       grid[2][0]==grid[2][1] && grid[2][1]==grid[2][2]	&& grid[2][0] != ' '||
	       grid[0][0]==grid[1][0] && grid[1][0]==grid[2][0] && grid[0][0] != ' '||
	       grid[0][1]==grid[1][1] && grid[1][1]==grid[2][1] && grid[0][1] != ' '||
	       grid[0][2]==grid[1][2] && grid[1][2]==grid[2][2] && grid[0][2] != ' '||
	       grid[2][0]==grid[1][1] && grid[1][1]==grid[0][2] && grid[2][0] != ' '||
	       grid[0][0]==grid[1][1] && grid[1][1]==grid[2][2] && grid[0][0] != ' '){

			if(display.getCounter() % 2== 0){
			System.out.println("Player 2 wins!!!");
			}else{System.out.println("Player 1 wins!!!");}
			return true;
		}else{return false;}
	}

	public boolean checkIfValidMove(char[][] grid){
		if(grid[position.getX()][position.getY()]!= ' '){
			display.placeTaken(grid);
			return false;
		}else{return true;}
	}

	public boolean checkDraw(int cnt){
		if(cnt==9 ){
			System.out.println("It's a DRAW!!!");
			return true;
		}else{return false;}
	}

	public char[][] getGrid() {
		return grid;
	}

	public Position getPosition(){
		return this.position;
	}

}
