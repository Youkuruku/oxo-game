public class Testing {

	Board board = new Board();

	void test(){
		//check_x
		assert(true == board.check_x('a'));
		assert(true == board.check_x('B'));
		assert(false == board.check_x('u'));
		assert(false == board.check_x('1'));
		assert(false == board.check_x('K'));
		assert(false == board.check_x(' '));

		//check_y
		assert(true == board.check_y('3'));
		assert(false == board.check_y('0'));
		assert(false == board.check_y('4'));
		assert(false == board.check_y('a'));
		assert(false == board.check_y('B'));
		assert(false == board.check_y(' '));
		assert(false == board.check_y('&'));

		//win
		char[][] winboard = {{'X','O','X'},{'O','X','O'},{'O','O','X'}};
		char[][] drawboard = {{'X','O','O'},{'O','X','X'},{'X','O','O'}};
		char[][] spaceboard = {{' ',' ',' '},{' ',' ',' '},{' ',' ',' '}};

		assert(true == board.win(winboard));
		assert(false == board.win(drawboard));
		assert(false == board.win(spaceboard));

		//checkDraw
		assert(true == board.checkDraw(9));
		assert(false == board.checkDraw(0));
		assert(false == board.checkDraw(-1));
		assert(false == board.checkDraw(12));
		assert(false == board.checkDraw(12367));

		//check fillBoard
		board.fillBoard();
		for(int i = 0; i<3; i++){
			for(int j= 0; j<3; j++){
				assert(board.getGrid()[i][j] == ' ');
			}
		}

		//checkIfValidMove
		char[][] board1 = {{'X','O','X'},{'O',' ',' '},{'O','O','X'}};
		char[][] board2 = {{'X','O','O'},{'O',' ','X'},{'X','O','O'}};
		board.getPosition().setX(1);
		board.getPosition().setY(2);
		assert(true == board.checkIfValidMove(board1));
		assert(false == board.checkIfValidMove(board2));

		System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" +
		"\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n   ALL TEST PASSED!!! \n\n\n");
	}
}
