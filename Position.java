public class Position {
	 private int x, y;

	/** DRY ;)
	 * Convert the string value to Board coordiantes.
	 * convert char x to ascii and deduct a==97 for x to get 0,1 or 2
	 * convert char y to number value and deduct 1 for y to get 0,1 or 2
	 * @param x
	 * @param y
	 */
	public void findPosition(char x, char y){
		this.x = (int)Character.toLowerCase(x) - (int)'a';
		this.y = Character.getNumericValue(y) -1;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y){
		this.y = y;
	}

	public void setX(int x){
		this.x = x;
	}


}
