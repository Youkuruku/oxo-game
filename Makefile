JFLAGS = -g
TARGET = Oxo
TARGET2 = -ea Oxo
SOURCES =  *.java
JC = javac

all: $(TARGET)

$(TARGET): $(SOURCES)
	$(JC) $(SOURCES) $(JFLAGS)

clean:
	rm -f $(TARGET)

run: all
	java $(TARGET)

test: all
	java $(TARGET2)
