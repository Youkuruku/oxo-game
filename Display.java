import java.util.Scanner;

public class Display{
	private char x, y;
	private int counter= 0;
	private Scanner scanner = new Scanner(System.in);
	private String coordinates;

	public void message(){
		if(counter % 2 == 0){
		System.out.println("\nPLAYER 1 give me the coordinates!\nROWS:(a,b or c)\nCOLUMNS: (1,2 or 3)");
		}else{
			System.out.println("\nPLAYER 2 give me the coordinates!\nROWS:(a,b or c)\nCOLUMNS: (1,2 or 3)");
		}
	}

	/*Ugly...but it works! */
	public void printBoard(char[][] grid){
		System.out.println("     1    2    3\n");
		System.out.println("   a   " +  grid[0][0] + " | " + grid[0][1] + " | " + grid[0][2]);
		System.out.println("      ___+___+___");
		System.out.println("   b   " + grid[1][0] + " | " + grid[1][1] + " | " + grid[1][2]);
		System.out.println("      ___+___+___");
		System.out.println("   c   " + grid[2][0] + " | " + grid[2][1] + " | " + grid[2][2]);
	}

	protected void get_input(char[][] grid){
		coordinates = "";
	   while(coordinates.length() != 2){
		   coordinates = scanner.next();
		   if(coordinates.length()!=2){
			   System.out.println("Coordinates must be exactly 2 characters long!");
		   }
	   }
		x = coordinates.charAt(0);
		y = coordinates.charAt(1);
		counter++;
	}

	public void outOfRangeChar(char[][] grid){
		System.out.println("Invalid ROW coordinates! Enter a, b or c DUMMIE!!!\n");
		counter--;
		printBoard(grid);
	}

	public void outOfRangeInt(char[][] grid){
		System.out.println("\nInvalid COLUMNS coordinates! Enter 1, 2 or 3 DUMMIE!!!");
		printBoard(grid);
		counter--;
	}

	public void placeTaken(char[][] grid){
		System.out.println("Position allready taken!");
		printBoard(grid);
		counter--;
	}

	public void newGameMessage(){
		System.out.println("Nice try! Would you like to play again???\n"
				+ "Type Y for Yes, N for No");
	}

	public boolean playNewGame(){
		String decision = "";
		while(!decision.equals("Y") || !decision.equals("N")){
			decision = scanner.next();
			if(decision.length()==1){
				if(decision.equals ( "Y")){
					return true;
				}
				if(decision.equals("N")){
					System.out.println("Ty for playing the English version of XOX..."
							+ "Have a nice day :)");
					return false;
				}
			}
			System.out.println("Wrong impmut!Try again...");
		}
		return false;
	}


	//GETTERS AND SETTERS
	protected char getX(){
		return x;
	}
	public char getY(){
		return y;
	}
	public int getCounter(){
		return counter;
	}
	public String getCoordinates(){
		return coordinates;
	}

}
